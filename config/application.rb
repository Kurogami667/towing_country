require_relative "boot"

require "rails/all"
require 'csv'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TowingCountry
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.generators do |g|
      g.assets false
      g.helper false
      g.stylesheets false
    end

    config.active_record.default_timezone = :local
    config.autoload_paths << Rails.root.join('lib')
    config.i18n.available_locales = %i[es en]
    config.i18n.default_locale = :en
    config.i18n.load_path += Dir[Rails.root.join('config/locales/**/*.{rb,yml}')]

    config.time_zone = 'Mexico City'
  end
end
